#!/usr/bin/env python3

import pathlib
import argparse
import functools

import numpy as np
import scipy.signal
import resampy.filters


FILTER_PARAMETERS = {
    'kaiser_best': {
        'num_zeros': 50,
        'precision': 13,
        'window': 'kaiser',
        'rolloff': 0.917347,
        'beta': 12.9846,
    },
    'kaiser_fast': {
        'num_zeros': 24,
        'precision': 9,
        'window': 'kaiser',
        'rolloff': 0.868212,
        'beta': 9.90322,
    },
}


def mkfilters(filter_parameters=FILTER_PARAMETERS, outdir=None):
    outdir = pathlib.Path(outdir)
    for name, parameters in filter_parameters.items():
        parameters = parameters.copy()

        num_zeros = parameters.pop('num_zeros')
        precision = parameters.pop('precision')
        window = parameters.pop('window')
        rolloff  = parameters.pop('rolloff')

        window = getattr(scipy.signal.windows, window)
        window = functools.partial(window, **parameters)
        half_win, precision, roll = resampy.filters.sinc_window(
            num_zeros=num_zeros,
            precision=precision,
            window=window,
            rolloff=rolloff,
        )

        outdir.mkdir(parents=True, exist_ok=True)
        filename = outdir / name
        np.savez(
            filename, half_window=half_win, precision=precision, rolloff=roll
        )


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'outdir', default=None,
         help='path to the directory to be used to store generated output files')

    args = parser.parse_args()
    mkfilters(FILTER_PARAMETERS, args.outdir) 
